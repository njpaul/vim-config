# Setup #
1. Checkout this repository into ~/.vim
2. Add "source ~/.vim/myvimrc" to ~/.vimrc
3. Create the "bundle" directory in ~/.vim
4. Clone the Vundle repository into ~/.vim/bundle
5. Run "vim +PluginInstall"
6. Do any additional plugin setup

The following plugins need additional configuration. See their documentation
for further installation instructions.

    * neco-ghc
    * YouCompleteMe

# Modify #
## Branch-only changes ##
1. Just commit to the branch

## Common changes ##
1. Make changes and test on a branch, but **DON'T COMMIT**
2. Stash the changes and switch to *master*
3. Unstash the changes
4. Merge from *master* to the branch(es) that require the changes

## Changes affecting multiple branches ##
If a change affects multiple branches, but is not a shared setting between all branches...

1. Make a branch off *master*
2. Merge the change to each branch that needs it
3. Delete the branch